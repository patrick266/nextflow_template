nextflow.enable.dsl=2
workflow {
    main:
        foo(
            params.input_file1,
            params.input_file2,
            params.hyper_param1,
            params.hyper_param2,
            params.output_directory_name
        )
}   

process foo {
    cpus = 1
    memory = 2.GB
    container = "AWS_ACCOUNT_ID.dkr.ecr.AWS_REGION.amazonaws.com/ECR_REGISTRY_NAME:ECR_REGISTRY_TAG" // ECR location of your Docker container
    
    input:
    path(input_file1)
    path(input_file2)
    val(hyper_param1)
    val(hyper_param2)
    val(output_directory_name)

    output:
    path("${output_directory_name}/*.csv") // Capture all csv outputs in the output directory
    path("*.log") // Capture the *.log output of the script in the root directory

    script:
    """
    /opt/conda/bin/python /myapp/foo.py \
        --input_file1 ${input_file1} \
        --input_file2 ${input_file2} \
        --hyper_param1 ${hyper_param1} \
        --hyper_param2 ${hyper_param2} \
        --output_directory ${output_directory_name}
    """
}
