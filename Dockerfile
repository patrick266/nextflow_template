# Define baseimage
FROM continuumio/miniconda3:latest

# Add repo contents
ADD . /myapp

# Install dependencies
RUN pip install -r /myapp/requirements.txt

# Set workdir
WORKDIR /myapp
